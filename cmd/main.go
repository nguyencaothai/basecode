package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"local.com/basecode/config"
	"local.com/basecode/internal/user"
	"local.com/basecode/pkg/middleware/authentication"
)

func main() {
	errLoadEnv := config.LoadEnv()
	if errLoadEnv != nil {
		fmt.Println(errLoadEnv)
	}

	errDatabaseInit := config.DatabaseInit()
	if errDatabaseInit != nil {
		fmt.Println(errDatabaseInit)
	}

	errMigrate := config.RunMigration()
	if errMigrate != nil {
		fmt.Println(errMigrate)
	}

	r := mux.NewRouter()

	userRoute := r.PathPrefix("/users").Subrouter()
	userRoute.Use(authentication.AuthMiddlware)
	userRoute.HandleFunc("/login", user.LoginFunc).Methods("POST")

	errStart := http.ListenAndServe(":8081", r)
	if errStart != nil {
		fmt.Println("error starting server at port 8081")
	}

}
